package com.upgrade.testcode;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.upgrade.testcode.model.Reservation;
import com.upgrade.testcode.model.User;

@Component
public class TestBuild {
	public Reservation buildReservation(LocalDate startDate, LocalDate endDate) {
		return addReservation("Alejandro", "Doglioli", "alejandro.doglioli@gmail.com", startDate, endDate);
	}

	public Reservation addReservation(String name, String lastName, String email, LocalDate startDate,
			LocalDate endDate) {

		User user = new User(name, lastName, email, null);

		return new Reservation(user, startDate, endDate);
	}
}
