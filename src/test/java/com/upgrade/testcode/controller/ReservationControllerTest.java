package com.upgrade.testcode.controller;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import com.upgrade.testcode.TestBuild;
import com.upgrade.testcode.model.Reservation;
import com.upgrade.testcode.repository.ReservationRepository;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationControllerTest {

	@Autowired
	private ReservationRepository reservationRepository;

	@Autowired
	private TestBuild build;

	@LocalServerPort
	int port;

	private String basePath = "/v1";

	private String controllerPath = "/reservations";

	@Before
	public void setUp() {
		RestAssured.port = port;
		RestAssured.basePath = basePath;
		RestAssured.defaultParser = Parser.JSON;
		reservationRepository.deleteAll();
	}

	@Test
	public void testReservationConcurrency() throws InterruptedException {
		int numberOfThreads = 10;
		ExecutorService service = Executors.newFixedThreadPool(10);
		CountDownLatch latch = new CountDownLatch(numberOfThreads);

		LocalDate startDate = LocalDate.now().plusDays(5);
		LocalDate endDate = LocalDate.now().plusDays(7);

		Reservation addedReservation = null;
		for (int i = 0; i < numberOfThreads; i++) {
			service.execute(() -> {
				reservationRepository.save(build.addReservation("Alejandro",
						"Doglioli", "alejandrodoglioli@gmail.com", startDate, endDate));
				latch.countDown();

			});
		}
		latch.await();
		assertEquals(reservationRepository.findAll().size(), 1);

	}

}
