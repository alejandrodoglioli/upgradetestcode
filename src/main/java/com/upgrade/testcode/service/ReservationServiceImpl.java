package com.upgrade.testcode.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.upgrade.testcode.exceptions.NotDaysAvailableException;
import com.upgrade.testcode.exceptions.NotExistReservationException;
import com.upgrade.testcode.model.Campsite;
import com.upgrade.testcode.model.Reservation;
import com.upgrade.testcode.repository.CampsiteRepository;
import com.upgrade.testcode.repository.ReservationRepository;

@Service
public class ReservationServiceImpl implements ReservationService {

	@Autowired
	private ReservationRepository reservationRepository;

	@Autowired
	private CampsiteRepository campsiteRepository;

	@Override
	public List<Reservation> getAllReservations() {
		return reservationRepository.findAll();
	}

	@Override
	public Optional<Reservation> getReservation(Long id) {
		return reservationRepository.findById(id);

	}

	@Override
	@Transactional()
	public Reservation addReservation(Reservation reservation) {

		List<Reservation> existReservation = getReservationBetweenDates(reservation.getCheckin(),
				reservation.getCheckout());

		if (existReservation != null) {
			String message = String.format("No available dates from %s to %s", reservation.getCheckin(),
					reservation.getCheckout());
			throw new NotDaysAvailableException(message);
		}

		Optional<Campsite> campsite = campsiteRepository.findById((long) 1);
		if (campsite.isPresent())
			reservation.setCampsite(campsite.get());
		return reservationRepository.save(reservation);
	}

	@Override
	@Transactional()
	public void updateReservation(Long id, Reservation reservation) {
		Reservation savedReservation = reservationRepository.getOne(reservation.getId());

	    if (savedReservation == null || savedReservation.getActive().equals(false)) {
	      String message = String.format("Reservation with id=%d not exist", reservation.getId());
	      throw new NotExistReservationException(message);
	    }
	    
		List<Reservation> existReservation = getReservationBetweenDates(reservation.getCheckin(),
				reservation.getCheckout());

		if (existReservation != null && existReservation.size() > 0) {
			String message = String.format("No available dates from %s to %s", reservation.getCheckin(),
					reservation.getCheckout());
			throw new NotDaysAvailableException(message);
		}
		
		reservationRepository.save(reservation);
	}

	@Override
	@Transactional()
	public boolean cancelReservation(Long id) {
		Reservation savedReservation = reservationRepository.getOne(id);
		savedReservation.setActive(false);
		
		reservationRepository.save(savedReservation);
		
		return !savedReservation.getActive();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Reservation> getReservationBetweenDates(LocalDate startDate, LocalDate endDate) {

		return reservationRepository.findAll().stream()
				.filter(x -> ((startDate.isBefore(x.getCheckin()) || startDate.isEqual(x.getCheckin()))
						&& (endDate.isAfter(x.getCheckin()) || endDate.isAfter(x.getCheckout())))
						|| (endDate.isBefore(x.getCheckout()) && endDate.isAfter(x.getCheckin())))
				.collect(Collectors.toList());
	}
}
