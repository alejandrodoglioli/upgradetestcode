package com.upgrade.testcode.service;

import java.util.List;
import java.util.Optional;

import com.upgrade.testcode.model.Campsite;

public interface CampsiteService {

	public List<Campsite> getAllUsers();

	public Optional<Campsite> getUser(Long id);

	public void addUser(Campsite campsite);

	public void updateReservation(Campsite campsite);

	public void deleteReservation(Long id);
}
