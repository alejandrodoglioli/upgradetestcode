package com.upgrade.testcode.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upgrade.testcode.model.Campsite;
import com.upgrade.testcode.repository.CampsiteRepository;

@Service
public class CampsiteServiceImpl implements CampsiteService {

	@Autowired
	private CampsiteRepository campsiteRepository;

	@Override
	public List<Campsite> getAllUsers() {
		return campsiteRepository.findAll();
	}

	@Override
	public Optional<Campsite> getUser(Long id) {
		return campsiteRepository.findById(id);
	}

	@Override
	public void addUser(Campsite campsite) {
		campsiteRepository.save(campsite);
	}

	@Override
	public void updateReservation(Campsite campsite) {
		campsiteRepository.save(campsite);
	}

	@Override
	public void deleteReservation(Long id) {
		campsiteRepository.deleteById(id);
	}
}
