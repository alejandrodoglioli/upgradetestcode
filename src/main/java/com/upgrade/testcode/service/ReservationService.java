package com.upgrade.testcode.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.upgrade.testcode.model.Reservation;

public interface ReservationService {

	public List<Reservation> getAllReservations();

	public List<Reservation> getReservationBetweenDates(LocalDate startDate, LocalDate endDate);

	public Optional<Reservation> getReservation(Long id);

	public Reservation addReservation(Reservation reservation);

	public void updateReservation(Long id, Reservation reservation);

	public boolean cancelReservation(Long id);
}
