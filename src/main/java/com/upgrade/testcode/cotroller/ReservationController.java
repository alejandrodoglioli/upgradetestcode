package com.upgrade.testcode.cotroller;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.upgrade.testcode.exceptions.DataNotFoundException;
import com.upgrade.testcode.model.Reservation;
import com.upgrade.testcode.service.ReservationServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1")
@Api(value = "upgradeapi", description = "Reservations api rest developed by Alejandro Doglioli for Upgrade.com")
public class ReservationController {

	@Autowired
	private ReservationServiceImpl reservationService;

	@RequestMapping(value = "/reservations", method = RequestMethod.GET)
	@ApiOperation(value = "Get a list of all reservations", response = List.class)
	public ResponseEntity<List<Reservation>> getAllReservations() {
		return new ResponseEntity(reservationService.getAllReservations(), HttpStatus.OK);
	}

	@RequestMapping(value = "/reservations/availability", method = RequestMethod.GET)
	@ApiOperation(value = "Get all available days between 2 dates, by default take one month.", response = List.class)
	public ResponseEntity<List<LocalDate>> getAvailableDays(
			@RequestParam(name = "start_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
			@RequestParam(name = "end_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate endDate) {

		if (startDate == null)
			startDate = LocalDate.now().plusDays(1);

		if (endDate == null)
			endDate = startDate.plusMonths(1);

		List<Reservation> reservations = reservationService.getReservationBetweenDates(startDate, endDate);

		List<LocalDate> availableDays = Stream.iterate(startDate, date -> date.plusDays(1))
				.limit(ChronoUnit.DAYS.between(startDate, endDate)).collect(Collectors.toList());

		reservations.forEach(b -> availableDays.removeAll(b.getReservationDays()));

		return new ResponseEntity<>(availableDays, HttpStatus.OK);
	}

	@GetMapping(value = "/reservations/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<Reservation> getBooking(@PathVariable() Long id) {

		Optional<Reservation> reservation = reservationService.getReservation(id);
		if (reservation.isPresent()) {
			return new ResponseEntity<Reservation>(reservation.get(), HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/reservations", method = RequestMethod.POST)
	@ApiOperation(value = "Create new reservation.", response = List.class)
	public ResponseEntity<Reservation> addReservation(@RequestBody @Valid Reservation reservation) {
		Reservation saved = reservationService.addReservation(reservation);

		return new ResponseEntity<>(saved, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/reservations/{id}", method = RequestMethod.PUT)
	@ApiOperation(value = "Update existent reservation.")
	public void updateReservation(@RequestBody Reservation reservation, @PathVariable Long id) {
		reservationService.updateReservation(id, reservation);
	}

	@RequestMapping(value = "/reservations/{id}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete or cancel reservation.", response = List.class)
	public ResponseEntity<Object> cancelReservation(@PathVariable Long id) {

		boolean cancelled = reservationService.cancelReservation(id);

		if (cancelled) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
