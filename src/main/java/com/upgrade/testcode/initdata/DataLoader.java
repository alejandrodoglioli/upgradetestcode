package com.upgrade.testcode.initdata;

import java.time.LocalDate;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.upgrade.testcode.model.Campsite;
import com.upgrade.testcode.model.Reservation;
import com.upgrade.testcode.model.User;
import com.upgrade.testcode.repository.CampsiteRepository;
import com.upgrade.testcode.repository.ReservationRepository;
import com.upgrade.testcode.repository.UserRepository;

@Component
public class DataLoader implements ApplicationRunner {

	private CampsiteRepository campsiteRepository;
	private UserRepository userRepository;
	private ReservationRepository reservationRepository;

	@Autowired
	public DataLoader(CampsiteRepository campsiteRepository, UserRepository userRepository,
			ReservationRepository reservationRepository) {
		this.campsiteRepository = campsiteRepository;
		this.userRepository = userRepository;
		this.reservationRepository = reservationRepository;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		Campsite campsite = new Campsite("Upgrade Campsite");
		

		User user1 = new User("Alejandro", "Doglioli", "alejandrodoglioli@gmail.com", null);
		User user2 = new User("Taryn", "Olson", "tarynolson@gmail.com", null);
		User user3 = new User("Juan", "Silval", "juansilval@gmail.com", null);
		User user4 = new User("Enrique", "Dacosta", "enriquedacosta@gmail.com", null);
		User user5 = new User("Matias", "Massigoge", "matiasmassigoge@gmail.com", null);
		User user6 = new User("Hernan", "Petersen", "hernanpetersen@gmail.com", null);
		User user7 = new User("Nico", "Cirigliano", "nicocirigliano@gmail.com", null);

		Reservation r1 = new Reservation(user1, LocalDate.of(2020, 9, 1), LocalDate.of(2020, 9, 3));
		Reservation r2 = new Reservation(user2, LocalDate.of(2020, 8, 20), LocalDate.of(2020, 8, 23));
		Reservation r3 = new Reservation(user3, LocalDate.of(2020, 8, 23), LocalDate.of(2020, 8, 24));
		Reservation r4 = new Reservation(user4, LocalDate.of(2020, 8, 24), LocalDate.of(2020, 8, 26));
		Reservation r5 = new Reservation(user5, LocalDate.of(2020, 8, 28), LocalDate.of(2020, 8, 29));
		Reservation r6 = new Reservation(user6, LocalDate.of(2020, 8, 15), LocalDate.of(2020, 8, 17));
		Reservation r7 = new Reservation(user7, LocalDate.of(2020, 8, 17), LocalDate.of(2020, 8, 18));

		/*reservationRepository.save(r1);
		reservationRepository.save(r2);
		reservationRepository.save(r3);
		reservationRepository.save(r4);
		reservationRepository.save(r5);
		reservationRepository.save(r6);
		reservationRepository.save(r7);*/
		
		campsite.getReservations().add(r1);
		campsite.getReservations().add(r2);
		campsite.getReservations().add(r3);
		campsite.getReservations().add(r4);
		campsite.getReservations().add(r5);
		campsite.getReservations().add(r6);
		campsite.getReservations().add(r7);
		
		campsiteRepository.save(campsite);
	}
}
