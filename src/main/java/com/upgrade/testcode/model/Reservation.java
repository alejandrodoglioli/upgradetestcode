package com.upgrade.testcode.model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.upgrade.testcode.model.base.GenericModel;
import com.upgrade.testcode.model.validator.daysaheadreservation.DaysAheadReservation;
import com.upgrade.testcode.model.validator.maxdaysreservation.MaxDaysReservation;
import com.upgrade.testcode.model.validator.validdatesreservation.ValidDatesReservation;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "RESERVATIONS")
@MaxDaysReservation
@DaysAheadReservation
@ValidDatesReservation
@JsonIgnoreProperties(ignoreUnknown=true)
public class Reservation extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(cascade = CascadeType.ALL)
	@ApiModelProperty(notes = "The campsite id")
	@JsonIgnore
	private Campsite campsite;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name="user_id")
	@ApiModelProperty(notes = "The user that did the reservation")
	private User user;

	@Column(name = "checking", nullable = false)
	@NotNull(message = "Check in date required")
	@Future(message = "Checkin must be a future date")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@ApiModelProperty(notes = "The checkin date")
	private LocalDate checkin;

	@Column(name = "checkout", nullable = false)
	@NotNull(message = "Check out date required")
	@Future(message = "Checkout must be a future date")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@ApiModelProperty(notes = "The checkout date")
	private LocalDate checkout;

	public Reservation() {
		super();
	}

	public Reservation(User user,
			@NotNull(message = "Check in date required") @Future(message = "Checkin must be a future date") LocalDate checkin,
			@NotNull(message = "Check out date required") @Future(message = "Checkout must be a future date") LocalDate checkout) {
		super();
		this.user = user;
		this.checkin = checkin;
		this.checkout = checkout;
		this.setInsertValues();
	}

	public Campsite getCampsite() {
		return campsite;
	}

	public void setCampsite(Campsite campsite) {
		this.campsite = campsite;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDate getCheckin() {
		return checkin;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

	public LocalDate getCheckout() {
		return checkout;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}

	@JsonIgnore
	public List<LocalDate> getReservationDays() {
		return Stream.iterate(this.checkin, date -> date.plusDays(1))
			    .limit(ChronoUnit.DAYS.between(checkin, checkout))
			    .collect(Collectors.toList());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((campsite == null) ? 0 : campsite.hashCode());
		result = prime * result + ((checkin == null) ? 0 : checkin.hashCode());
		result = prime * result + ((checkout == null) ? 0 : checkout.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (campsite == null) {
			if (other.campsite != null)
				return false;
		} else if (!campsite.equals(other.campsite))
			return false;
		if (checkin == null) {
			if (other.checkin != null)
				return false;
		} else if (!checkin.equals(other.checkin))
			return false;
		if (checkout == null) {
			if (other.checkout != null)
				return false;
		} else if (!checkout.equals(other.checkout))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

}
