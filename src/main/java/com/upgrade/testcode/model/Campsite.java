package com.upgrade.testcode.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.upgrade.testcode.model.base.GenericModel;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "CAMPSITE")
public class Campsite extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static LocalTime DEFAULT_CHECKIN = LocalTime.of(12, 0);
	private final static LocalTime DEFAULT_CHECKOUT = LocalTime.of(12, 0);

	@Column(name = "name")
	@ApiModelProperty(notes = "The campsite name")
	private String name;

	@Column(name = "checkinTime")
	@ApiModelProperty(notes = "The checkin time")
	private LocalTime checkinTime;

	@Column(name = "checkoutTime")
	@ApiModelProperty(notes = "The checkout date")
	private LocalTime checkoutTime;

	@OneToMany(targetEntity = Reservation.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "campsite_id", referencedColumnName = "id")
	@JsonIgnore
	private List<Reservation> reservations = new ArrayList<>();

	public Campsite() {
		super();
	}

	public Campsite(String name) {
		super();
		this.name = name;
		this.checkinTime = DEFAULT_CHECKIN;
		this.checkoutTime = DEFAULT_CHECKOUT;
		this.setInsertValues();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalTime getCheckinTime() {
		return checkinTime;
	}

	public void setCheckinTime(LocalTime checkinTime) {
		this.checkinTime = checkinTime;
	}

	public LocalTime getCheckoutTime() {
		return checkoutTime;
	}

	public void setCheckoutTime(LocalTime checkoutTime) {
		this.checkoutTime = checkoutTime;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((checkinTime == null) ? 0 : checkinTime.hashCode());
		result = prime * result + ((checkoutTime == null) ? 0 : checkoutTime.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campsite other = (Campsite) obj;
		if (checkinTime == null) {
			if (other.checkinTime != null)
				return false;
		} else if (!checkinTime.equals(other.checkinTime))
			return false;
		if (checkoutTime == null) {
			if (other.checkoutTime != null)
				return false;
		} else if (!checkoutTime.equals(other.checkoutTime))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;

		return true;
	}

}
