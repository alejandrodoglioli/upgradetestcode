package com.upgrade.testcode.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.upgrade.testcode.model.base.GenericModel;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "USERS")
public class User extends GenericModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "name")
	@ApiModelProperty(notes = "The user name")
	private String name;

	@Column(name = "lastName")
	@ApiModelProperty(notes = "The user lastname")
	private String lastName;

	@Column(name = "email")
	@ApiModelProperty(notes = "The user email")
	private String email;

	@OneToOne(mappedBy = "user", fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.ALL }, optional = true)
	@JsonIgnore
	private Reservation reservation;

	public User() {
		super();
	}

	public User(String name, String lastName, String email, Reservation reservation) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.reservation = reservation;
		this.setInsertValues();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((reservation == null) ? 0 : reservation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (reservation == null) {
			if (other.reservation != null)
				return false;
		} else if (!reservation.equals(other.reservation))
			return false;
		return true;
	}

}
