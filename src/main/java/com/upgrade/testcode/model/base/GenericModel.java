package com.upgrade.testcode.model.base;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import io.swagger.annotations.ApiModelProperty;

@MappedSuperclass
public class GenericModel implements Serializable {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The generated ID")
	private Long id;

	@Column(name = "active")
	@ApiModelProperty(notes = "Indicate if and record is active or not")
	private Boolean active;

	@Column(name = "date_update")
	@ApiModelProperty(notes = "Updated date")
	private LocalDate dateUpdate;

	@Column(name = "date_insert")
	@ApiModelProperty(notes = "Inserted date")
	private LocalDate dateInsert;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public LocalDate getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(LocalDate dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public LocalDate getDateInsert() {
		return dateInsert;
	}

	public void setDateInsert(LocalDate dateInsert) {
		this.dateInsert = dateInsert;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((dateInsert == null) ? 0 : dateInsert.hashCode());
		result = prime * result + ((dateUpdate == null) ? 0 : dateUpdate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenericModel other = (GenericModel) obj;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (dateInsert == null) {
			if (other.dateInsert != null)
				return false;
		} else if (!dateInsert.equals(other.dateInsert))
			return false;
		if (dateUpdate == null) {
			if (other.dateUpdate != null)
				return false;
		} else if (!dateUpdate.equals(other.dateUpdate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setInsertValues() {
		setDateInsert(LocalDate.now());
		setActive(true);
	}

	public void setUpdateValues() {
		setDateUpdate(LocalDate.now());
	}

}
