package com.upgrade.testcode.model.validator.maxdaysreservation;

import java.time.Period;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.upgrade.testcode.model.Reservation;

public class MaxDaysReservationValidator implements ConstraintValidator<MaxDaysReservation, Reservation> {

	  @Override
	  public void initialize(MaxDaysReservation constraintAnnotation) {

	  }

	  @Override
	  public boolean isValid(Reservation reservation, ConstraintValidatorContext constraintValidatorContext) {
	    if (reservation == null) {
	      return true;
	    }

	    return Period.between(reservation.getCheckin(), reservation.getCheckout()).getDays() <= 3;
	  }
	}
