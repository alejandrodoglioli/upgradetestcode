package com.upgrade.testcode.model.validator.validdatesreservation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidDatesReservationValidator.class})
@Documented
public @interface ValidDatesReservation {
	String message() default "Checin date should be before checkout date.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
