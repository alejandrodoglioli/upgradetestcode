package com.upgrade.testcode.model.validator.maxdaysreservation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {MaxDaysReservationValidator.class})
@Documented
public @interface MaxDaysReservation {
	String message() default "Reservation stay can be less or equal than 3 days";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
