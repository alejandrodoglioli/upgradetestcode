package com.upgrade.testcode.model.validator.daysaheadreservation;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.upgrade.testcode.model.Reservation;

public class DaysAheadReservationValidator implements ConstraintValidator<DaysAheadReservation, Reservation> {

	  @Override
	  public void initialize(DaysAheadReservation constraintAnnotation) {

	  }

	  @Override
	  public boolean isValid(Reservation reservation, ConstraintValidatorContext constraintValidatorContext) {
	    if (reservation == null) {
	      return true;
	    }

	    return LocalDate.now().isBefore(reservation.getCheckin().minusDays(1)) && LocalDate.now().isAfter(reservation.getCheckin().minusMonths(1)); 
	  }
	}
