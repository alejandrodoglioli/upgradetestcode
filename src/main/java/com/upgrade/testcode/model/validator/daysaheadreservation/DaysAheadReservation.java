package com.upgrade.testcode.model.validator.daysaheadreservation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {DaysAheadReservationValidator.class})
@Documented
public @interface DaysAheadReservation {
	String message() default "The campsite can be reserved minimum 1 day(s) ahead of arrival and up to 1 month in advance.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
