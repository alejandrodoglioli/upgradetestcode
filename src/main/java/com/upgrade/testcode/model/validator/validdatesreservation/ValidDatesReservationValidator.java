package com.upgrade.testcode.model.validator.validdatesreservation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.upgrade.testcode.model.Reservation;

public class ValidDatesReservationValidator implements ConstraintValidator<ValidDatesReservation, Reservation> {

	  @Override
	  public void initialize(ValidDatesReservation constraintAnnotation) {

	  }

	  @Override
	  public boolean isValid(Reservation reservation, ConstraintValidatorContext constraintValidatorContext) {
	    if (reservation == null) {
	      return true;
	    }

	    return reservation.getCheckin().isBefore(reservation.getCheckout());
	  }
	}
