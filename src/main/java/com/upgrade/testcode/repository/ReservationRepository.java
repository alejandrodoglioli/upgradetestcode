package com.upgrade.testcode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.upgrade.testcode.model.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Long> {

}
