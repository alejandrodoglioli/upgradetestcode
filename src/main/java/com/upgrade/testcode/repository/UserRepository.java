package com.upgrade.testcode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.upgrade.testcode.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

}
