package com.upgrade.testcode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.upgrade.testcode.model.Campsite;

@Repository
public interface CampsiteRepository extends JpaRepository<Campsite,Long> {

}
