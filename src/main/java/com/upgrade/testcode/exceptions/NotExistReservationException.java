package com.upgrade.testcode.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class NotExistReservationException extends RuntimeException {

	public NotExistReservationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
