package com.upgrade.testcode.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.upgrade.testcode.cotroller")).paths(PathSelectors.regex("/v1/reservations.*"))
				.build().apiInfo(metaData()); 

	}

	private ApiInfo metaData() {
		ApiInfo apiInfo = new ApiInfo("Upgrade Test Code", "Spring Boot REST API for Upgrade", "1.0",
				"Terms of service",
				new Contact("Alejandro Doglioli", "/", "alejandrodoglioli@gmail.com"),
				"", "", new ArrayList());
		return apiInfo;

	}

}