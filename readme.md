# Campsite Reservation

Developed campsite reservation by Alejandro Doglioli

## Description
An underwater volcano formed a new small island in the Pacific Ocean last month. All the conditions on the island seems perfect and it was
decided to open it up for the general public to experience the pristine uncharted territory.
The island is big enough to host a single campsite so everybody is very excited to visit. In order to regulate the number of people on the island, it
was decided to come up with an online web application to manage the reservations. You are responsible for design and development of a REST
API service that will manage the campsite reservations.
To streamline the reservations a few constraints need to be in place: 

- The campsite will be free for all.
- The campsite can be reserved for max 3 days.
- The campsite can be reserved minimum 1 day(s) ahead of arrival and up to 1 month in advance.
- Reservations can be cancelled anytime.
- For sake of simplicity assume the check-in & check-out time is 12:00 AM

### System Requirements
- The users will need to find out when the campsite is available. So the system should expose an API to provide information of the
availability of the campsite for a given date range with the default being 1 month.
- Provide an end point for reserving the campsite. The user will provide his/her email & full name at the time of reserving the campsite
along with intended arrival date and departure date. Return a unique booking identifier back to the caller if the reservation is successful.
- The unique booking identifier can be used to modify or cancel the reservation later on. Provide appropriate end point(s) to allow
modification/cancellation of an existing reservation
- Due to the popularity of the island, there is a high likelihood of multiple users attempting to reserve the campsite for the same/overlapping
date(s). Demonstrate with appropriate test cases that the system can gracefully handle concurrent requests to reserve the campsite.
- Provide appropriate error messages to the caller to indicate the error cases.
- In general, the system should be able to handle large volume of requests for getting the campsite availability.
- There are no restrictions on how reservations are stored as as long as system constraints are not violated.

## Technologies

    Java 1.8
    Spring Boot
    JPA
    Hibernate
    H2
    Springfox Swagger


## Running Project
- URL to access: http://localhost:8080/v1/reservations/

### With maven
	git clone https://alejandrodoglioli@bitbucket.org/alejandrodoglioli/upgradetestcode.git
	cd upgradetestcode
	mvn spring-boot:run

### with jar
	git clone https://alejandrodoglioli@bitbucket.org/alejandrodoglioli/upgradetestcode.git
	cd upgradetestcode
	mvn package -DskipTests java -jar target/upgradetestcode-0.0.1-SNAPSHOT.jar


## Accessing Data in H2 Database

### H2 console
URL to access H2 console: http://localhost:8080/h2-console


- JDBC URL: jdbc:h2:mem:campsite;MODE=MySQL
- User Name: root
- Password: root

## Swagger

URL to access Swagger UI: http://localhost:8080/swagger-ui.html


## Solution

### The campsite can be reserved for max 3 days. 
I have created a custom validator /testcode/src/main/java/com/upgrade/testcode/model/validator/maxdaysreservation/MaxDaysReservationValidator.java to fix that

### The campsite can be reserved minimum 1 day(s) ahead of arrival and up to 1 month in advance.
I have created a custom validator /testcode/src/main/java/com/upgrade/testcode/model/validator/daysaheadreservation/DaysAheadReservationValidator.java to fix that

### The users will need to find out when the campsite is available. So the system should expose an API to provide information of the availability of the campsite for a given date range with the default being 1 month.

The endpoint can be called with the following url: localhost:8080/vi/reservations/availability it return a list of days available between 2 dates, by default take 1 month ahead


### Provide an end point for reserving the campsite. 
localhost:8080/v1/reservations and we pass a Reservation object in the body will create a new reservation

### Due to the popularity of the island, there is a high likelihood of multiple users attempting to reserve the campsite for the same/overlapping date(s). Demonstrate with appropriate test cases that the system can gracefully handle concurrent requests to reserve the campsite.

I provided a test case in /testcode/src/test/java/com/upgrade/testcode/TestcodeApplicationTests.java to test concurrency. In order to handle concurrency I added @Transactional anotation in each repository method that access to database.



